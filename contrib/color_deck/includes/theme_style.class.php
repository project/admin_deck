<?php

class theme_style {
  var $plugin;
  var $settings;

  function init($plugin) {
    $this->plugin = $plugin;
    $settings = variable_get('color_deck_style', array());
    if (!$settings) {
      foreach ($plugin['palette'] as $preset => $values) {
        $settings['palette'][$preset] = $values['default_value'];
      }
      $settings['name'] = $plugin['name'];
      variable_set('color_deck_style', $settings);
    }
    $this->settings = $settings;
  }

  function page_build(&$page) {
    ctools_include('stylizer');
    ctools_stylizer_add_css($this->plugin, $this->settings);
  }
}