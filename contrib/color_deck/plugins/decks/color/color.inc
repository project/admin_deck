<?php


function color_deck_color_admin_deck_decks() {
  $plugin = array(
    'permission' => 'color_deck_color_permission',
    'access' => 'color_deck_color_access',
    'render' => 'drupal_get_form',
    'content' => 'color_deck_color_form',
    'content submit' => 'color_deck_color_form_submit',
    'page build' => 'color_deck_color_page_build',
    'icon' => 'color-icon.png',
    'handle text' => t('Colorize'),
  );
  return $plugin;
}

function color_deck_color_permission() {
  return array(
    'administer theme style' => array(
      'title' => t('Administer theme style'), 
      'description' => t('Access the theme style system.'),
    ),
  );
}

function color_deck_color_access() {
  //return user_access('administer theme style');
  return TRUE;
}

function color_deck_color_form($form, &$form_state) {
  global $theme_info;
  if (!isset($theme_info->info['theme_style']['default'])) {
    return;
  }
  ctools_add_js('stylizer');
  ctools_add_css('stylizer');
  drupal_add_js('misc/farbtastic/farbtastic.js');
  drupal_add_css('misc/farbtastic/farbtastic.css');
  ctools_add_css('color-deck', 'color_deck');
  ctools_add_js('color-deck', 'color_deck');
  ctools_add_js('color-deck', 'color_deck');
  $theme_style = color_deck_get_active_plugin($theme_info);
  $plugin = $theme_style->plugin;
  $settings = variable_get('color_deck_style', array());
  $settings['key'] = ($settings['key'] == 'custom') ? 0 : $settings['key'];
  $styles = color_deck_get_theme_styles($theme_info->name);
  $form['styles'] = array(
    '#type' => 'select',
    '#title' => t('Theme Styles'),
    '#options' => $styles,
    '#default_value' => $settings['key'],
  );
  if (!empty($plugin['palette'])) {
    $form['top box']['color'] = array(
      '#type' => 'fieldset',
      '#title' => t('Palette'),
      '#attributes' => array('id' => 'ctools_stylizer_color_scheme_form', 'class' => array('ctools-stylizer-color-edit')),
      '#theme' => 'ctools_stylizer_color_scheme_form',
    );

    $form['top box']['color']['palette']['#tree'] = TRUE;

    foreach ($plugin['palette'] as $key => $color) {
      if (empty($settings['palette'][$key])) {
        $settings['palette'][$key] = $color['default_value'];
      }

      $form['top box']['color']['palette'][$key] = array(
        '#type' => 'textfield',
        '#title' => $color['label'] . ':',
        '#default_value' => $settings['palette'][$key],
        '#size' => 8,
      );
    }
  }
  $form['color_plugin'] = array(
    '#type' => 'value',
    '#value' => $plugin,
  );
  if (!empty($plugin['form']) && function_exists($plugin['form'])) {
    $form = $plugin['form']($form, $form_state);
  }
  return $form;
}

function color_deck_color_form_submit($form, &$form_state) {
  $plugin = $form_state['values']['color_plugin'];
  $settings = variable_get('color_deck_style', array());
  ctools_include('stylizer');
  ctools_stylizer_cleanup_style($plugin, $settings);
  foreach ($form_state['input']['palette'] as $preset => $color) {
    $form_state['settings']['palette'][$preset] = $color;
  }
  $settings = $form_state['settings'];
  $settings['key'] = $form_state['input']['styles'] ? $form_state['input']['styles'] : 'custom';
  
  if ($settings['key'] != 'custom') {
    ctools_include('plugins');
    $plugins = ctools_get_plugins('color_deck', 'theme_style');
    foreach ($plugins as $color_plugin) {
      if ($settings['key'] == $color_plugin['key']) {
        $settings['name'] = $color_plugin['name'];
        break;
      }
    }
  }
  else {
    $settings['name'] = 'custom';
  }
  if (!empty($plugin['form submit']) && function_exists($plugin['form submit'])) {
    $plugin['form submit']($form, $form_state);
  }
  variable_set('color_deck_style', $settings);
}
