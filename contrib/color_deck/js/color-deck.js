(function ($) {
  Drupal.behaviors.color_deck = {
    attach: function (context) {
      Drupal.CTools.Stylizer.resetScheme = function () {
        $('#edit-styles').val(0);
      }

      $('#edit-styles').change(function () {
        var colors = this.options[this.selectedIndex].value;
        console.log(colors);
        if (colors != '') {
          colors = colors.split(',');
          for (i in colors) {
            Drupal.CTools.Stylizer.callback(Drupal.CTools.Stylizer.inputs[i], colors[i], false, true);
          }
        }
      });
    }
  };
})(jQuery);